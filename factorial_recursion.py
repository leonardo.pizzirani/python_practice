from functools import lru_cache
import time

def time_decorator(function):
    def wrapper(name):
        begin = time.time()
        function()
        end = time.time()
        print(name, "\t", end-begin)
    return wrapper

@time_decorator
def naive():
    def fact(n):
        if n == 1:
            return 1
        else:
            return n*fact(n-1)

    for i in range(1, 35):
        fact(i)

@time_decorator
def memo():
    cache = dict()

    def fact(n):
        if n in cache:
            return cache[n]
        elif n == 1:
            x = 1
        else:
            x = n*fact(n-1)
            cache[n] = x
        return x

    for i in range(1, 10000):
        fact(i)

@time_decorator
def iterf():
    def fact(n):
        tot = 1
        for i in range(2, n):
            tot *= i
        return tot

    for i in range(1, 10000):
        fact(i)

@time_decorator
def lru():
    @lru_cache()
    def fact(n):
        if n==1:
            x = 1
        else:
            x = n*fact(n-1)
        return x

    for i in range(1, 10000):
        fact(i)

naive("naive, 35")
memo("memo, 10000")
iterf("iterf, 10000")
lru("lru, 10000")
