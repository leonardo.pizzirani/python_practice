from functools import lru_cache
import time

def time_decorator(function):
    def wrapper(name):
        begin = time.time()
        function()
        end = time.time()
        print(name, "\t", end-begin)
    return wrapper

@time_decorator
def naive():
    def fibo(n):
        if n == 0:
            return 0
        elif n == 1:
            return 1
        else:
            return fibo(n-1) + fibo(n-2)

    for i in range(1, 35):
        fibo(i)

@time_decorator
def memo():
    cache = dict()

    def fibo(n):
        if n in cache:
            return cache[n]
        elif n == 0:
            x = 0
        elif n == 1:
            x = 1
        else:
            x = fibo(n-1) + fibo(n-2)
            cache[n] = x
        return x

    for i in range(1, 10000):
        fibo(i)

@time_decorator
def iterf():
    def fibo(n):
        sum1 = 1
        sum2 = 1
        for i in range(2, n):
            tmp = sum2
            sum2 += sum1
            sum1 = tmp
        return sum2

    for i in range(1, 10000):
        fibo(i)

@time_decorator
def lru():
    @lru_cache()
    def fibo(n):
        if n==0:
            x = 0
        elif n==1:
            x = 1
        else:
            x = fibo(n-1) + fibo(n-2)
        return x

    for i in range(1, 10000):
        fibo(i)

naive("naive, 35")
memo("memo, 10000")
iterf("iterf, 10000")
lru("lru, 10000")
