from functools import lru_cache
from collections import deque
import time


def time_decorator(function):
    def wrapper(name, *args):
        begin = time.time()
        function(*args)
        end = time.time()
        print(f"{name}\t{end-begin}\n")
    return wrapper


@time_decorator
def naive(n_disk, source, dest, aux):
    stack = {}
    stack[source] = deque()
    stack[dest] = deque()
    stack[aux] = deque()
    for i in range(n_disk, 0, -1):
        stack[source].append(i)
    print(stack)

    def hanoi(n_disk, source, dest, aux):
        if n_disk > 0:
            hanoi(n_disk-1, source, aux, dest)
            stack[dest].append(stack[source].pop())
            print(stack)
            hanoi(n_disk-1, aux, dest, source)

    hanoi(n_disk, source, dest, aux)


@time_decorator
def lru(n_disk, source, dest, aux):

    @lru_cache
    def hanoi(n_disk, source, dest, aux):
        if n_disk > 0:
            hanoi(n_disk-1, source, aux, dest)
            print(f"{source} → {dest}")
            hanoi(n_disk-1, aux, dest, source)

    hanoi(n_disk, source, dest, aux)


n_disk = 4
naive("NAIVE:\t", n_disk, 1, 2, 3)
lru("LRU (WRONG):", n_disk, 1, 2, 3)
